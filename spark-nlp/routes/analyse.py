import datetime
import hashlib
import json

import sparknlp
from dateutil.parser import parse
from flask import Flask, request
from flask_restplus import Api, Resource, fields
from pyspark.ml import PipelineModel
from pyspark.sql.types import StringType, StructType, StructField

app = Flask(__name__)
api = Api(app,
          version="0.1",
          title="Canada votes 2019",
          description="Entities Identification and Sentiment Classification")

sentiment_namespace = api.namespace('analyse', description='Sentiment Classification and Entity Identification')
api.add_namespace(sentiment_namespace)

# the tweet input model
jsonl_tweets = api.model("Tweet JSON Lines", {
    "tweet_lines": fields.String(description="The JSON String one per line", min_length=1),
    "make_lines": fields.Boolean(default=True)
})

# the response model
cols = [StructField("id", StringType(), True),
        StructField("text", StringType(), True),
        StructField("timestamp", StringType(), True)
        ]

schema = StructType(cols)

sc = sparknlp.start()

# Load from locally saved pipelines for faster response (download and unzip from https://nlp.johnsnowlabs.com/docs/en/pipelines
pipeline_entities = PipelineModel.load('resources/pipelines/recognize_entities_dl_en_2.1.0_2.4_1562946909722')
pipeline_sentiment = PipelineModel.load('resources/pipelines/analyze_sentiment_en_2.1.0_2.4_1563204637489')


# This loads the most comprehensive text portion of the tweet
# Where "data" is an individual tweet, treated as JSON / dict
def getText(data):
    # Try for extended text of original tweet, if RT'd (streamer)
    try:
        text = data['retweeted_status']['extended_tweet']['full_text']
    except:
        # Try for extended text of an original tweet, if RT'd (REST API)
        try:
            text = data['retweeted_status']['full_text']
        except:
            # Try for extended text of an original tweet (streamer)
            try:
                text = data['extended_tweet']['full_text']
            except:
                # Try for extended text of an original tweet (REST API)
                try:
                    text = data['full_text']
                except:
                    # Try for basic text of original tweet if RT'd
                    try:
                        text = data['retweeted_status']['text']
                    except:
                        # Try for basic text of an original tweet
                        try:
                            text = data['text']
                        except:
                            # Nothing left to check for
                            text = ''
    return text.replace('\n', '')


def make_json(string_list):
    dict_list = []
    columns = ["id", "tweet_id", "timestamp", "sentence", "sentiment", "entities"]
    for row in string_list:
        row = dict(zip(columns, row))
        dict_list.append(row)

    return dict_list


@sentiment_namespace.route('')
@api.doc()
@api.expect(jsonl_tweets)
class Sentiment(Resource):
    def post(self):
        """Obtain the sentiments and entities"""
        content = request.get_data().decode("utf-8")
        tweet_array = content.split('\n')

        # ONLY EXTRACT MEANINGFUL PARTS
        rows = []
        for tweet in tweet_array:
            if len(tweet) > 0:
                tweet = json.loads(tweet)
                rows.append((tweet['id'], getText(tweet), tweet['created_at']))

        # CONVERT IT TO A SPARK DF & GET THE SENTIMENT
        df = sc.createDataFrame(rows, schema=schema)
        sentiment = pipeline_sentiment.transform(df)
        entities = pipeline_entities.transform(df)

        # ASSEMBLE THE RESPONSE
        sentiment_result = sentiment.select("id", "timestamp", "sentence.result", "sentiment.result").collect()
        entities_result = entities.select("entities.metadata", "entities.result").collect()

        response = []
        for s, e in zip(sentiment_result, entities_result):
            sentence_array = s[2]
            sentiment_array = s[3]

            s_cnt = 0
            for sentence, sentiment in zip(sentence_array, sentiment_array):

                # make an unique id from the sentence string
                entry_id = hashlib.md5((sentence.encode())).hexdigest()

                # get all entities for the current sentence
                ent_arr = []
                for m in e[0]:
                    s_id = int(m["sentence"])
                    if s_cnt == s_id:
                        ent_dict = {"name": e[1][int(m["chunk"])], "type": m["entity"]}
                        ent_arr.append(ent_dict)

                # collect the elements
                epoch_time = parse(s["timestamp"]).timestamp()
                timestamp = datetime.datetime.utcfromtimestamp(epoch_time).strftime('%Y-%m-%dT%H:%M:%SZ')
                entry = entry_id, s["id"], timestamp, sentence, sentiment, ent_arr
                response.append(entry)
                s_cnt = s_cnt + 1

        return make_json(response)


if __name__ == "__main__":
    app.run(debug=True, use_reloader=False, threaded=True, host='0.0.0.0', port=5000)
