import json
import os

import boto3
import requests

# VPC internal execution
# endpoint = 'canada-votes:5000/analyse'

# internet execution
endpoint = 'http://canada-votes-1159255985.us-east-1.elb.amazonaws.com:80/analyse'

os.environ['AWS_PROFILE'] = "default"
os.environ['AWS_DEFAULT_REGION'] = "us-east-1"
os.environ['AWS_ACCESS_KEY_ID'] = "AKIAIJBQKFEG64DSXWJA"
os.environ['AWS_SECRET_ACCESS_KEY'] = "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"

bucket_name = 'canadavotes2019-tweetsbucket-8uz0tkeqc0q0'
source_prefix = 'raw'
s3_resource = boto3.resource('s3')
firehose = boto3.client('firehose')
firehose_name = 'CanadaVotes2019-analysys'
bucket = s3_resource.Bucket(name=bucket_name)


def analyse(body):
    r = requests.post(url=endpoint, data=body)
    if r.status_code == 200:
        result = r.text
    else:
        result = "failed analysis"
        print(result)
    return result


count = 0

for obj in bucket.objects.filter(Prefix=source_prefix):
    body = obj.get()['Body'].read()
    response = analyse(body)

    # put the response to S3 firehose
    if response != "failed analysis":
        for entry in json.loads(response):
            firehose.put_record(DeliveryStreamName=firehose_name, Record={'Data': json.dumps(entry) + '\n'})

    # count = count + 1
    # if count > 2:
    #     break
