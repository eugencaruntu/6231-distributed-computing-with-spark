#!/bin/bash

# build the spark docker container
#docker build -t spark-node spark

# pull the image as is
docker pull thiagolcmelo/spark-debian
docker tag thiagolcmelo/spark-debian spark-node

# make a network
docker network create --driver bridge spark-network

# run a master
docker run -d -t \
             --name master \
             --network spark-network \
             spark-node
docker exec master start-master

# run worker 1
docker run -d -t \
             --name worker-1 \
             --network spark-network \
             spark-node
docker exec worker-1 start-slave spark://master:7077

# run worker 2
docker run -d -t \
             --name worker-2 \
             --network spark-network \
             spark-node
docker exec worker-2 start-slave spark://master:7077

# run worker 3
docker run -d -t \
             --name worker-3 \
             --network spark-network \
             spark-node
docker exec worker-3 start-slave spark://master:7077