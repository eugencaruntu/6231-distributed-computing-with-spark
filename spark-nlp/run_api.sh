#!/bin/bash
app="canada-votes-2019"

# build only if image is missing
if [[ "$(docker images -q ${app}:latest 2>/dev/null)" == "" ]]; then
  docker build -t ${app} spark-nlp
fi

# create a network (same as spark cluster)
#docker network create --driver bridhe spark-network

# run the api container and expose all its ports on the mentioned network
docker run -d -t \
  --name ${app} \
  --network host \
  --publish-all=true \
  ${app}:latest
