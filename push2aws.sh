#!/bin/bash
app="canada-votes-2019"

# build only if image is missing
if [[ "$(docker images -q ${app}:latest 2>/dev/null)" == "" ]]; then
  docker build -t ${app} spark-nlp
fi

# tag the image for aws repo
docker tag ${app}:latest 249197905124.dkr.ecr.us-east-1.amazonaws.com/${app}:latest

# get the credentials and authenticate
login_credentials=$(aws ecr get-login --no-include-email --region us-east-1)
${login_credentials}

# push to aws repo
docker push 249197905124.dkr.ecr.us-east-1.amazonaws.com/${app}:latest

# force update the service
aws ecs update-service --cluster canada-votes --service canada-votes --force-new-deployment
