import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.twitter.TwitterUtils;
import org.apache.spark.streaming.twitter.TwitterUtils;
import org.spark_project.guava.collect.ImmutableList;
import org.spark_project.guava.collect.ImmutableMap;
import scala.Tuple2;
import twitter4j.Status;
import twitter4j.auth.Authorization;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;


import java.util.Arrays;
import java.util.Map;

public class TwitterTopHashtags {

    public static void main(String[] args) {

        // configure Spark nodes
        SparkConf sparkConf = new SparkConf().setAppName("Twitter Top #tags").setMaster("local[100]");

        // set context and duration
        JavaStreamingContext streamingContext = new JavaStreamingContext(sparkConf, Duration.apply(10000));

        // authorization configuration
        Configuration conf = new ConfigurationBuilder()
                .setOAuthConsumerKey("BTmtMO7D16LNY0jZXzmwtuKCy")
                .setOAuthConsumerSecret("IPOkroiapSXS4SZ4gGPzG70EINYo2Kt48evFY9Z5ABFU9tSGjK")
                .setOAuthAccessToken("1152343684358316034-qmpkobdRdzVkvglGuYTLC9px0wBx5K")
                .setOAuthAccessTokenSecret("dh1ZItkwW2r3pMKbGz6DMUfFshmOmhTKFxLdWToX2LgSV")
                .build();

        Authorization twitterAuth = new OAuthAuthorization(conf);

        // get only English tweets and split to words, filter #tags
        JavaReceiverInputDStream<Status> inputDStream = TwitterUtils.createStream(streamingContext, twitterAuth, new String[]{});
        JavaDStream<Status> statuses = inputDStream.filter(t -> "en".equalsIgnoreCase(t.getLang()));
        JavaDStream<Status> politics = inputDStream.filter(t -> "en".equalsIgnoreCase(t.getLang()));
        JavaDStream<String> englishTweets = statuses.map(Status::getText);
        JavaDStream<String> words = englishTweets.flatMap(t -> Arrays.asList(t.split(" ")).iterator());
        JavaDStream<String> hashTags = words.filter((Function<String, Boolean>) word -> word.startsWith("#"));

        // count by #tag and reduce by #tag then reduce by #tag and time frame
        // TODO: read and persist data to account for drawbacks of window processing: http://www.michael-noll.com/blog/2013/01/18/implementing-real-time-trending-topics-in-storm/
        JavaPairDStream<String, Integer> tuples = hashTags.mapToPair(in -> new Tuple2<>(in, 1));
        JavaPairDStream<String, Integer> counts = tuples.reduceByKeyAndWindow(
                (Function2<Integer, Integer, Integer>) Integer::sum,
                Durations.seconds(30), Durations.seconds(10)
        );

        // swap k/v to get counts as key
        JavaPairDStream<Integer, String> swappedCounts = counts.mapToPair(
                (PairFunction<Tuple2<String, Integer>, Integer, String>) Tuple2::swap
        );

        // sort by key in descending order
        JavaPairDStream<Integer, String> sortedCounts = swappedCounts.transformToPair(
                (Function<JavaPairRDD<Integer, String>, JavaPairRDD<Integer, String>>) x -> x.sortByKey(false));


        // Persist in ES
        // https://github.com/elastic/elasticsearch-hadoop#apache-spark
      /*  Map<String, ?> numbers = ImmutableMap.of("one", 1, "two", 2);
        Map<String, ?> airports = ImmutableMap.of("OTP", "Otopeni", "SFO", "San Fran");

        SparkContext sc = new SparkContext();
        JavaRDD<Map<String, ?>> javaRDD = sc.parallelize(ImmutableList.of(numbers, airports));
        JavaEsSpark.saveToEs(javaRDD, "spark/docs");*/

        sortedCounts.print();

        // action !
        streamingContext.start();

        try {
            streamingContext.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
